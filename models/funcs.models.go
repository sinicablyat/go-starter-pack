package models

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
	"github.com/tkanos/gonfig"

	// Уничтожить

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // Get postgres dialect
	//

	"database/sql"

	_ "github.com/lib/pq"
)

// Configuration -- Config from configuration/config.json
type Configuration struct {
	Port       string
	DbType     string
	DbUser     string
	DbPort     string
	DbName     string
	DbPassword string
	RedisPort  string
	ServerName string
}

// RedisConnect return redis.Conn
func RedisConnect() redis.Conn {
	configuration := Configuration{}
	err := gonfig.GetConf("configuration/config.json", &configuration)

	c, err := redis.Dial("tcp", configuration.RedisPort)
	CheckErr(err)
	// defer c.Close() // If don't work delete this
	return c
}

/* Some funcs to work with psql */

// GetGormDB return a connected to db
func GetGormDB() *gorm.DB {
	configuration := Configuration{}
	_ = gonfig.GetConf("configuration/config.json", &configuration)

	db, err := gorm.Open(configuration.DbType, GetConnectionString())
	CheckErr(err)
	// defer db.Close() // If don't work delete this
	return db
}

// GetConnectionString return connection to db string
func GetConnectionString() string {
	configuration := Configuration{}
	_ = gonfig.GetConf("configuration/config.json", &configuration)

	result := fmt.Sprintf("user=%s password=%s dbname=%s  sslmode=disable", configuration.DbUser, configuration.DbPassword, configuration.DbName)
	return result
}

// CheckErr check for error in models things
func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

// MigrateModels create tables in empty database
func MigrateModels() {
	// Создаем миграции с помощью gorm. Это легче и привычнее
	// На скорость не влияет, так как делается единожды

	// db := GetGormDB()

	// // List of create tables
	// db.CreateTable(&User{})
	// db.CreateTable(&Profile{})
}

// DataTools toolkit for db-things
type DataTools struct {
	DB *sql.DB
}

// GetDB return DataTools
func GetDB() DataTools {
	dbinfo := GetConnectionString()

	db, err := sql.Open("postgres", dbinfo)
	CheckErr(err)

	// defer db.Close()

	dt := DataTools{DB: db}
	return dt
}
