package models

import (
	"database/sql"
	"errors"
	"log"
	"strconv"
	"time"

	_ "github.com/jinzhu/gorm/dialects/postgres" // Get postgres dialect
	_ "github.com/lib/pq"
)

// ParseProfile parse user from sql.Rows to Profile{} struct
func (tools *DataTools) ParseProfile(rows *sql.Rows) (Profile, error) {
	var (
		id        uint
		createdAt time.Time
		nick      string
		firstName string
		lastName  string
		host      string
	)

	err := rows.Scan(&id, &createdAt, &nick, &firstName, &lastName, &host)
	if err != nil {
		return Profile{}, err
	}

	profile := Profile{ID: id, CreatedAt: createdAt, Nick: nick, FirstName: firstName, LastName: lastName}
	return profile, nil
}

// CreateProfile create profile in db by user struct{}
func (tools *DataTools) CreateProfile(profile Profile) (Profile, error) {
	db := tools.DB

	var (
		id        uint
		createdAt time.Time
		nick      string
		firstName string
		lastName  string
	)

	_, err := db.Exec("INSERT INTO profiles (created_at, nick, first_name, last_name) VALUES(CURRENT_TIMESTAMP, $1, $2, $3);", profile.Nick, profile.FirstName, profile.LastName)
	if err != nil {
		log.Println(err)
		return Profile{}, err
	}

	err = db.QueryRow("SELECT * FROM profiles WHERE nick = $1;", profile.Nick).Scan(&id, &createdAt, &nick, &firstName, &lastName)
	if err != nil {
		log.Println(err)
		return Profile{}, err
	}

	profile = Profile{ID: id, CreatedAt: createdAt, Nick: nick, FirstName: firstName, LastName: lastName}
	return profile, nil
}

// GetProfileFromUser return Profile{} by User{}
func (tools *DataTools) GetProfileFromUser(user User) (Profile, error) {
	db := tools.DB

	rows, err := db.Query("SELECT * FROM users WHERE id = $1", user.ProfileID)
	if err != nil {
		return Profile{}, err
	}

	if rows == nil {
		err = errors.New("PROFILE NOT FOUND!")
		return Profile{}, err
	}

	profile, err := tools.ParseProfile(rows)
	if err != nil {
		return Profile{}, err
	}

	return profile, nil
}

// FindProfileByID find profile by idQ
func (tools *DataTools) FindProfileByID(idQ string) (Profile, error) {
	db := tools.DB

	idInt, err := strconv.ParseInt(idQ, 10, 64)
	if err != nil {
		return Profile{}, err
	}

	rows, err := db.Query("SELECT * FROM profiles WHERE id = $1", idInt)
	if err != nil {
		return Profile{}, err
	}

	if rows == nil {
		err = errors.New("Profile not found!")
		return Profile{}, err
	}

	profile, err := tools.ParseProfile(rows)
	if err != nil {
		return Profile{}, err
	}

	return profile, nil
}

// FindProfileByNick find profile by idQ
func (tools *DataTools) FindProfileByNick(nickQ string) (Profile, error) {
	db := tools.DB

	rows, err := db.Query("SELECT * FROM profiles WHERE nick = $1;", nickQ)
	if err != nil {
		return Profile{}, err
	}

	if rows == nil {
		err = errors.New("Profile not found!")
		return Profile{}, err
	}

	profile, err := tools.ParseProfile(rows)
	if err != nil {
		return Profile{}, err
	}

	return profile, nil
}
