package models

import (
	"database/sql"
	"log"
	"strconv"
	"time"
	// "time"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	_ "github.com/jinzhu/gorm/dialects/postgres" // Get postgres dialect
	_ "github.com/lib/pq"
)

// CreateUser create user in db by user struct{}
func (tools *DataTools) CreateUser(user User) (User, error) {
	db := tools.DB

	var (
		id           uint
		createdAt    time.Time
		email        string
		nick         string
		firstName    string
		lastName     string
		passwordHash string
		profileID    uint
	)

	_, err := db.Exec("INSERT INTO users (created_at, email, nick, first_name, last_name, password_hash, profile_id) VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6);", user.Email, user.Nick, user.FirstName, user.LastName, user.PasswordHash, user.ProfileID)
	if err != nil {
		log.Println(err)
		return User{}, err
	}

	err = db.QueryRow("SELECT * FROM users WHERE nick = $1;", user.Nick).Scan(&id, &createdAt, &email, &nick, &firstName, &lastName, &passwordHash, &profileID)
	if err != nil {
		log.Println(err)
		return User{}, err
	}

	user = User{ID: id, CreatedAt: createdAt, Email: email, Nick: nick, FirstName: firstName, LastName: lastName, PasswordHash: passwordHash, ProfileID: profileID}
	return user, nil
}

// FindUserByNick find user by nick
func (tools *DataTools) FindUserByNick(nickQ string) (User, error) {
	db := tools.DB

	var (
		id           uint
		createdAt    time.Time
		email        string
		nick         string
		firstName    string
		lastName     string
		passwordHash string
		profileID    uint
	)

	err := db.QueryRow("SELECT * FROM users WHERE nick = $1", nickQ).Scan(&id, &createdAt, &email, &nick, &firstName, &lastName, &passwordHash, &profileID)
	if err != nil {
		return User{}, err
	}

	user := User{ID: id, CreatedAt: createdAt, Email: email, Nick: nick, FirstName: firstName, LastName: lastName, PasswordHash: passwordHash, ProfileID: profileID}
	return user, nil
}

// FindUserByID find user by idQ
func (tools *DataTools) FindUserByID(idQ string) (User, error) {
	db := tools.DB

	var (
		id           uint
		createdAt    time.Time
		email        string
		nick         string
		firstName    string
		lastName     string
		passwordHash string
		profileID    uint
	)

	idInt, err := strconv.ParseInt(idQ, 10, 64)
	if err != nil {
		return User{}, err
	}

	err = db.QueryRow("SELECT * FROM users WHERE id = $1", idInt).Scan(&id, &createdAt, &email, &nick, &firstName, &lastName, &passwordHash, &profileID)
	if err != nil {
		return User{}, err
	}

	user := User{ID: id, CreatedAt: createdAt, Email: email, Nick: nick, FirstName: firstName, LastName: lastName, PasswordHash: passwordHash, ProfileID: profileID}
	return user, nil
}

// ParseUser parse user from sql.Rows to User{} struct
func (tools *DataTools) ParseUser(rows *sql.Rows) (User, error) {
	var (
		id           uint
		createdAt    time.Time
		email        string
		nick         string
		firstName    string
		lastName     string
		passwordHash string
		profileID    uint
	)

	err := rows.Scan(&id, &createdAt, &email, &nick, &firstName, &lastName, &passwordHash, &profileID)
	if err != nil {
		return User{}, err
	}

	user := User{ID: id, CreatedAt: createdAt, Email: email, Nick: nick, FirstName: firstName, LastName: lastName, PasswordHash: passwordHash, ProfileID: profileID}
	return user, nil
}

// GetProfileFromAnother get profile from another server by nick
func GetProfileFromAnother(userTwoServer string, userTwoNick string) (Profile, string) {
	reqUser, err := http.PostForm(fmt.Sprintf("%s/users/getuserbynick", userTwoServer),
		url.Values{"nick": {userTwoNick}})
	if err != nil {
		return Profile{}, "ERROR IN QUERY!"
	}

	reqUserBody, err := ioutil.ReadAll(reqUser.Body)
	if err != nil {
		return Profile{}, "ERROR IN QUERY!"
	}

	reqUserParsed := Profile{}
	err = json.Unmarshal(reqUserBody, &reqUserParsed)
	if err != nil {
		return Profile{}, "ERROR WITH VALUES!"
	}

	return reqUserParsed, ""
}
