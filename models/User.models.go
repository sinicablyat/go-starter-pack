package models

import (
	// "time"

	"time"

	_ "github.com/jinzhu/gorm/dialects/postgres" // Get postgres dialect
	_ "github.com/lib/pq"
)

// User model
type User struct {
	ID           uint      `gorm:"primary_key"`
	CreatedAt    time.Time `json:"-"`
	Email        string    `gorm:"type:varchar(100);unique_index;"` // Email
	Nick         string    `gorm:"type:varchar(50);unique_index;"`  // NickName
	FirstName    string    `gorm:"type:varchar(100);"`              // FirstName
	LastName     string    `gorm:"type:varchar(100);"`              // LastName
	PasswordHash string

	Profile   Profile `gorm:"foreignkey:ProfileID;association_foreignkey:Refer;"`
	ProfileID uint
}

// Profile is a public information of User
// Here Can be a some relation ships with Posts, Comments, etc...
type Profile struct {
	ID        uint      `gorm:"primary_key"`
	CreatedAt time.Time `json:"-"`

	Nick      string `gorm:"type:varchar(50);unique_index;"` // UserNick
	FirstName string `gorm:"type:varchar(100);"`             // UserFirstName
	LastName  string `gorm:"type:varchar(100);"`             // UserLastName

	/*
		Разные связи типа Постов и коментариев сюда
	*/
}
