package models

import(
	"time"

	_ "github.com/jinzhu/gorm" // Get gorm struct
	_ "github.com/jinzhu/gorm/dialects/postgres" // Get postgres dialect
	_ "github.com/lib/pq"
)

// Model is basic columns for all models
type Model struct {
	ID 		  uint 	     `gorm:"primary_key"`
	CreatedAt time.Time	 `json:"-"`
  	UpdatedAt time.Time  `json:"-"`
  	DeletedAt *time.Time `json:"-"`
}