package controllers

import (
	"database/sql"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"../models"
	"github.com/gomodule/redigo/redis"
	"github.com/tkanos/gonfig"
	"github.com/valyala/fasthttp"
)

/*
	Login
*/

// LoginRouteGet is auth/register GET router
func LoginRouteGet(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(ctx, "Login Route here!")
}

// LoginRoutePost is auth/login POST router
func LoginRoutePost(ctx *fasthttp.RequestCtx) {
	dt := models.GetDB()
	db := dt.DB

	conn := models.RedisConnect()

	var (
		uemail    = string(ctx.FormValue("email")[:])
		upassword = string(ctx.FormValue("password")[:])
	)

	var (
		id           uint
		createdAt    time.Time
		email        string
		nick         string
		firstName    string
		lastName     string
		passwordHash string
		profileID    uint
	)

	err := db.QueryRow("SELECT * FROM users WHERE email = $1 AND password_hash = $2", uemail, upassword).Scan(&id, &createdAt, &email, &nick, &firstName, &lastName, &passwordHash, &profileID)
	if err != nil {
		ThrowErr("ERROR IF QUERY!", 401, ctx)
		return
	}
	user := models.User{ID: id, CreatedAt: createdAt, Email: email, Nick: nick, FirstName: firstName, LastName: lastName, PasswordHash: passwordHash, ProfileID: profileID}

	/*
		Создание записи в redis
	*/
	tokenID := RandStringBytes(16)
	token := RandStringBytes(128)

	// Добавляем запись
	_, err = conn.Do("SET", fmt.Sprintf(`tokens:%s`, tokenID), fmt.Sprintf(`%d-%s`, user.ID, token))
	if err != nil {
		ThrowErr("SOMETHING GOES WRONG! ERROR 400! 3", 400, ctx)
		return
	}

	db.Close()

	/*
		Даем токен*/
	result := []byte(fmt.Sprintf(`{ "Message": "Logined successffully!", "token": "%s-%s" }`, tokenID, token))
	ctx.Response.SetStatusCode(200)
	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetBody(result)
	return
}

/*
	Register
*/

// RegisterRouteGet is auth/register GET router
func RegisterRouteGet(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(ctx, "Register Route here!")
}

// RegisterRoutePost is auth/register POST router
func RegisterRoutePost(ctx *fasthttp.RequestCtx) {
	dt := models.GetDB()
	db := dt.DB

	conn := models.RedisConnect()

	configuration := models.Configuration{}
	_ = gonfig.GetConf("configuration/config.json", &configuration)

	var (
		unick      = string(ctx.FormValue("nick")[:])
		uemail     = string(ctx.FormValue("email")[:])
		ufirstname = string(ctx.FormValue("firstname")[:])
		ulastname  = string(ctx.FormValue("lastname")[:])
		upassword  = string(ctx.FormValue("password")[:])
	)

	if len(unick) == 0 || len(uemail) == 0 || len(ufirstname) == 0 || len(ulastname) == 0 || len(upassword) == 0 {
		ThrowErr("Values is uncorrect!", 400, ctx)
		return
	}

	_, err := db.Query("SELECT * FROM users WHERE Nick = $1 OR Email = $2", unick, uemail)
	if err != nil {
		if err != sql.ErrNoRows { // Простите, за говнокод
			ThrowErr("User with this nick/email already exist!", 401, ctx)
			return
		}
	}

	preProfile := models.Profile{Nick: unick, FirstName: ufirstname, LastName: ulastname}
	profile, err := dt.CreateProfile(preProfile)
	if err != nil {
		ThrowErr("ERROR ON CREATING PROFILE!", 401, ctx)
		return
	}

	preUser := models.User{Nick: unick, FirstName: ufirstname, LastName: ulastname, PasswordHash: upassword, ProfileID: profile.ID, Email: uemail}
	user, err := dt.CreateUser(preUser)
	if err != nil {
		ThrowErr("ERROR ON CREATING USER!", 401, ctx)
		return
	}

	db.Close()

	/*
		Создание записи в redis
	*/
	tokenID := RandStringBytes(16)
	token := RandStringBytes(128)

	// Добавляем запись
	_, err = conn.Do("SET", fmt.Sprintf(`tokens:%s`, tokenID), fmt.Sprintf(`%d-%s`, user.ID, token))
	if err != nil {
		ThrowErr("SOMETHING GOES WRONG! ERROR 400! 3", 400, ctx)
		return
	}

	/*
		Даем токен*/
	result := []byte(fmt.Sprintf(`{ "Message": "Registered successffully!", "token": "%s-%s" }`, tokenID, token))
	ctx.Response.SetStatusCode(200)
	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetBody(result)
	return
}

// DeleteTokenRoute is auth/logout route for deleting auth-token
func DeleteTokenRoute(ctx *fasthttp.RequestCtx) {
	var (
		token = string(ctx.FormValue("token")[:])
		conn  = models.RedisConnect()
	)

	if len(token) == 0 {
		ThrowErr("TOKEN NOT FOUND! ERROR 404!", 404, ctx)
		return
	}

	tokenParsed := strings.Split(token, "-")
	tokenRedis, err := redis.String(conn.Do("GET", fmt.Sprintf(`tokens:%s`, tokenParsed[0])))
	if err != nil {
		ThrowErr("SOMETHING GOES WRONG! ERROR 400!", 400, ctx)
		return
	}

	tokenRedisParsed := strings.Split(tokenRedis, "-")
	if tokenRedisParsed[1] != tokenParsed[1] {
		ThrowErr("SOMETHING GOES WRONG! ERROR 400!", 400, ctx)
		return
	}

	_, err = redis.String(conn.Do("DEL", fmt.Sprintf(`tokens:%s`, tokenParsed[0])))
	if err != nil {
		ThrowErr("SOMETHING GOES WRONG! ERROR 400!", 400, ctx)
		return
	}

	/*
		Даем ответ*/
	result := []byte(`{ "Message": "Token delete successffully!" }`)
	ctx.Response.SetStatusCode(200)
	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetBody(result)
}

// RandStringBytes Return a random string witn X chars
func RandStringBytes(n int) string {
	rand.Seed(time.Now().UTC().UnixNano())
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// ResponseTokenBody struct to return JSON response with message and authTOKEN
type ResponseTokenBody struct {
	message string
	token   string
}
