package controllers

import (
	"fmt"

	"github.com/valyala/fasthttp"

	"../models"
)

// RouteIndex return a index page
// "/"
func RouteIndex(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(ctx, "User-Agent is %q\n", ctx.UserAgent())
}

// RouteVip retrun a VIP page
// "/vip"
func RouteVip(ctx *fasthttp.RequestCtx) {
	dt := models.GetDB()
	user := GetUserFromToken(ctx, dt)

	fmt.Fprintf(ctx, "There is a VIP Palace, %s!", user.Nick)
}
