package controllers

import (
	"fmt"
	// "math/rand"

	// "log"
	"strings"

	"../models"
	"github.com/gomodule/redigo/redis"
	"github.com/valyala/fasthttp"
)

/*
	Most used funcs
*/

// ThrowErr -- little rewrited standart ctx.Error() for api needs
func ThrowErr(msg string, statusCode int, ctx *fasthttp.RequestCtx) {
	ctx.Response.Reset()
	ctx.SetStatusCode(statusCode)
	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.SetBodyString(fmt.Sprintf(`{ "Message": "%s" }`, msg))
}

/*
	User checking
*/

// GetUserFromToken return user from auth cookies
func GetUserFromToken(ctx *fasthttp.RequestCtx, dt models.DataTools) (user models.User) {
	var (
		token = string(ctx.FormValue("token")[:])
		conn  = models.RedisConnect()
	)

	if len(token) == 0 {
		ThrowErr("TOKEN NOT FOUND! ERROR 404!", 404, ctx)
		return
	}

	tokenParsed := strings.Split(token, "-")
	tokenRedis, err := redis.String(conn.Do("GET", fmt.Sprintf(`tokens:%s`, tokenParsed[0])))
	if err != nil {
		ThrowErr("SOMETHING GOES WRONG! ERROR 400!", 400, ctx)
		return
	}

	tokenRedisParsed := strings.Split(tokenRedis, "-")
	if tokenRedisParsed[1] != tokenParsed[1] {
		ThrowErr("SOMETHING GOES WRONG! ERROR 400!", 400, ctx)
		return
	}

	user, err = dt.FindUserByID(tokenRedisParsed[0])
	if err != nil {
		ThrowErr("USER NOT FOUND!", 403, ctx)
		return
	}

	return
}

// CheckAuth return TRUE if token exist, FALSE if not
func CheckAuth(ctx *fasthttp.RequestCtx) bool {
	token := string(ctx.FormValue("token")[:])
	if len(token) == 0 {
		return false
	}
	return true
}

// RemoveElementsFromArr remove first n elements from array
func RemoveElementsFromArr(s []string, i int) []string {
	for j := 0; j < i; j++ {
		s = RemoveElementsFromArr(s, 0)
	}
	return s
}

// RemoveElFromArr remove n element from array
func RemoveElFromArr(s []string, i int) []string {
	return append(s[:i], s[i+1:]...)
}

// ErrorWithCode struct to return error with body and code
type ErrorWithCode struct {
	Body string
	Code int
}
