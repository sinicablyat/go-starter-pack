package main

import (

	// "fmt"
	"log"

	"github.com/tkanos/gonfig"

	"github.com/valyala/fasthttp"

	"github.com/buaazp/fasthttprouter"

	"./models"
	"./routes"
)

type Configuration struct {
	Port       string
	DbType     string
	DbUser     string
	DbPort     string
	DbName     string
	DbPassword string
	RedisPort  string
	ServerName string
}

func main() {
	router := fasthttprouter.New()
	models.MigrateModels()

	routes.InitializeRouter(router)

	s := &fasthttp.Server{
		Handler:            router.Handler,
		MaxRequestBodySize: 200 * 1024 * 1024,
	}

	configuration := Configuration{}
	_ = gonfig.GetConf("configuration/config.json", &configuration)

	if err := s.ListenAndServe(configuration.Port); err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}
}
