package routes

import(
	"fmt"
	"time"
	// "math/rand"

	"github.com/valyala/fasthttp"
	"../models"
)

/*
	AuthCheck
*/

// IsLogged auth check abstraction
func IsLogged(ctx *fasthttp.RequestCtx) {
	exist := CheckAuth(ctx)
	IsLoggedGo(exist, ctx)
}
// IsNotLogged auth check abstraction
func IsNotLogged(ctx *fasthttp.RequestCtx) {
	exist := CheckAuth(ctx)
	IsNotLoggedGo(exist, ctx)
}
// CheckAuth return TRUE if authCookie exist, FALSE if not
func CheckAuth(ctx *fasthttp.RequestCtx) (bool) {
	cookieExist := ctx.Request.Header.Cookie(authCookieName)
	if cookieExist == nil {
		return false
	}
	return true
}
// IsLoggedGo redirect when true
func IsLoggedGo(a bool, ctx *fasthttp.RequestCtx) {
	if a {
		return
	}
	fmt.Fprintf(ctx, "U're DON'T LOGINED! ERROR 401!")
	ctx.Redirect("/", 401)
	return
}
// IsNotLoggedGo redirect when false
func IsNotLoggedGo(a bool, ctx *fasthttp.RequestCtx) {
	if a {
		fmt.Fprintf(ctx, "U're ALREADY LOGINED! ERROR 401!")
		ctx.Redirect("/", 401)
		return
	}
	return
}

/*
	Logout
*/

// LogOutUser logout users
func LogOutUser(ctx *fasthttp.RequestCtx) {
	db := models.GetDB()
	cookieExist := ctx.Request.Header.Cookie(authCookieName)

	userSessionExist := models.UserSession{}
	db.Where(&models.UserSession{LoginDevice: fmt.Sprintf("%q", ctx.UserAgent()), Key: string(cookieExist) }).First(&userSessionExist)
	if len(userSessionExist.LoginDevice) != 0 {
		db.Delete(&userSessionExist)
	}

	delCookie := fasthttp.Cookie{}
	delCookie.SetKey(authCookieName)
	delCookie.SetHTTPOnly(true)
	delCookie.SetPath("/")
	delCookie.SetExpire(time.Unix(1, 0))
	ctx.Response.Header.SetCookie(&delCookie)
}