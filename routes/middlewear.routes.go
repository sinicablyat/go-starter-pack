package routes

import (
	"github.com/valyala/fasthttp"

	// "github.com/buaazp/fasthttprouter"
	"../models"
	"../controllers"
)

// IsLogged is auth middlewear
func IsLogged(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		db := models.GetDB()
		currentuser := controllers.GetUserFromToken(ctx, db)
		
		if len(currentuser.Nick) != 0 {
			h(ctx)
			return
		}

		// Request Basic Authentication otherwise
		ctx.Error("YOU'RE NOT AUTHORIZED!", fasthttp.StatusUnauthorized)
	})
}

// IsNotLogged is auth middlewear
func IsNotLogged(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		db := models.GetDB()
		currentuser := controllers.GetUserFromToken(ctx, db)
		
		if len(currentuser.Nick) == 0 {
			h(ctx)
			return
		}

		// Request Basic Authentication otherwise
		ctx.Error("YOU'RE AUTHORIZED!", fasthttp.StatusUnauthorized)
	})
}