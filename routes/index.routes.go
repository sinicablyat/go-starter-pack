package routes

import(
	"github.com/buaazp/fasthttprouter"
	// "github.com/valyala/fasthttp"

	"../controllers"
)

// InitializeIndexRoutes initialze a AUTH routes
// "example.com/..."
func InitializeIndexRoutes(r *fasthttprouter.Router) {

	r.ServeFiles("/staticfiles/*filepath", "./public/static")

	r.GET("/", controllers.RouteIndex)
	r.GET("/vip", IsLogged(controllers.RouteVip))
	r.POST("/vip", IsLogged(controllers.RouteVip))
}