package routes

import (
	"github.com/buaazp/fasthttprouter"

	"../controllers"
)

// InitializeAuthRoutes initialze a AUTH routes
// "example.com/auth/..."
func InitializeAuthRoutes(r *fasthttprouter.Router) {

	r.GET("/auth/register", IsNotLogged(controllers.RegisterRouteGet))   // Register GET
	r.POST("/auth/register", IsNotLogged(controllers.RegisterRoutePost)) // Register POST

	r.GET("/auth/login", IsNotLogged(controllers.LoginRouteGet))   // Login GET
	r.POST("/auth/login", IsNotLogged(controllers.LoginRoutePost)) // Login POST

	r.GET("/auth/logout", IsLogged(controllers.DeleteTokenRoute))  // Delete token GET
	r.POST("/auth/logout", IsLogged(controllers.DeleteTokenRoute)) // Delete token POST
}
