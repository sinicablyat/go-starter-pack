package routes

import (
	"github.com/buaazp/fasthttprouter"
)

// InitializeRouter return routes initialization
func InitializeRouter(r *fasthttprouter.Router) {

	InitializeIndexRoutes(r) // Index "example.com/..."
	InitializeAuthRoutes(r)  // AUTH "example.com/auth/..."

}
